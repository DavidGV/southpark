package SouthPark;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;



public class Puntuacion extends SouthPark implements Comparable{
	String fecha;
	String nombre;
	int puntuacion;
	public Puntuacion(int puntuacion, String nombre, String fecha) {
		this.puntuacion = puntuacion;
		this.nombre = nombre;
		this.fecha=fecha;
	}
	
	public static void guardarPuntuacion(String path) throws IOException {
		File f = new File(path);
		FileWriter fw = new FileWriter(f,true); //modo append
		BufferedWriter bw = new BufferedWriter(fw);
			//escribir en buffer
		for (Puntuacion p : SouthPark.puntuaciones) {
			String linea = p.puntuacion+","+p.nombre+","+p.fecha;
			bw.write(linea);
			bw.newLine();
		}
		//escribir en fichero
		bw.flush();
		bw.close();
	}
	public static void leerPuntuacion(String path) throws IOException {
		File f = new File(path);
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		SouthPark.puntuaciones = new ArrayList<Puntuacion>();
		while(br.ready()) {
			String s = br.readLine();
			String[] spl = s.split(",");
			Puntuacion p = new Puntuacion(Integer.parseInt(spl[0]), spl[1], spl[2]);
			SouthPark.puntuaciones.add(p);
		}
		Collections.sort(SouthPark.puntuaciones); 
		Collections.reverse(SouthPark.puntuaciones);
		br.close();
		for (Puntuacion p : SouthPark.puntuaciones) {
			System.out.println(p);
		}
	}
	
	

	@Override
	public String toString() {
		return "Puntuacion [ puntuacion=" + puntuacion+ ", nombre=" + nombre + ", fecha=" + fecha + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + puntuacion;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Puntuacion other = (Puntuacion) obj;
		if (puntuacion != other.puntuacion)
			return false;
		return true;
	}

	@Override
	public int compareTo(Object p) {
		Puntuacion otro = (Puntuacion) p;
		if(this.puntuacion>otro.puntuacion) {
			return 1;
		}else if(this.puntuacion<otro.puntuacion) {
			return -1;
		}else {
			return 0;
		}
	}
}
