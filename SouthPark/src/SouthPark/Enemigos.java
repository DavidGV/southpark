package SouthPark;

import java.util.ArrayList;
import java.util.Random;

/**
 * Esta clase esta creada basicamente para todo lo que este relacionado con los
 * enemigos dentro del juego
 * 
 * @author David Gil Vazquianez
 * @version 1.0
 *
 */
public class Enemigos extends Personajes{
	static int ids = 0;
	int id;
	int cooldownPatoRey;
	@Override
	public String toString() {
		return "Enemigos [id=" + id + ", name=" + name + ", x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2
				+ "]";
	}

	public Enemigos(String name, int x1, int y1, int x2, int y2, double angle, String[] path) {
		super(name, x1, y1, x2, y2, angle, path);
		if (name.equals("Garrison")) {
			hp = 5;
		} else if (name.equals("patoRey")){
			hp = 10;
			cooldown2 = 0;
			ids++;
			id = ids;
			direccion = 'i';
			cooldown = 0;
			cooldownPatoRey = 250;
		} else {
			hp = 3;
			cooldown2 = 0;
		}
		/*ids++;
		id = ids;
		direccion = 'i';
		cooldown = 75;
        */
	}

	public Enemigos(String name, int x1, int y1, String[] path) {
		super(name, x1, y1, x1 + 55, y1 + 75, 0, path);
		hp = 3;
		direccion = 'i';
		cooldown = 75;

	}
	public Enemigos(String name, int x1, int y1, double angle, String[] path) {
		super(name, x1, y1, x1 + 55, y1 + 125, 0, path);
		hp = 3;
		cooldown2 = 0;
		ids++;
		id = ids;
		direccion = 'i';
		cooldown = 0;

	}

	/**
	 * Le quita vida a los enemigos, en caso de que hp==0 los elimina
	 */
	public boolean golpear() {
		hp--;
		if (hp == 0) {
			if (SouthPark.pasarLvl2) {
				SouthPark.contEnemigos++;
				System.out.println("enemigos eliminados:"+SouthPark.contEnemigos);
			}
			if (name.equals("Garrison") || name.equals("patoRey")) {
				SouthPark.score+=500;
				System.out.println("Score1:"+SouthPark.score);
			}else {
				SouthPark.score+=100;
				System.out.println("Score2:"+SouthPark.score);
			}
			delete();
			return true;
		}
		return false;
	}
	public void updatePatoRey() {
		// TODO Auto-generated method stub
		if (cooldownPatoRey < 300)
			cooldownPatoRey++;
	}

	/**
	 * El Boss persigue a Cartman
	 */
	public void perseguir() {
		if ((x1 - Cartman.getCartman().x1) < 1000 || SouthPark.pasarLvl2) {
			if (((Cartman.getCartman().x1 + ((Cartman.getCartman().x2 - Cartman.getCartman().x1) / 2))
					- (x1 + ((x2 - x1) / 2))) < 0) {
				if (SouthPark.pasarLvl2) {
					x1 -= 2;
					x2 -= 2;
					for (BolasDeNieve p : SouthPark.mapas.get(1).tiros) {
						if(p.id == id) {
							p.x -= 2;	
						}	
					}
				} else {
					x1--;
					x2--;
				}

				if (name.equals("Butters")) {
					currentImg = 0;
				}
			} else if (((Cartman.getCartman().x1 + ((Cartman.getCartman().x2 - Cartman.getCartman().x1) / 2))
					- (x1 + ((x2 - x1) / 2))) > 0) {
				if (SouthPark.pasarLvl2) {
					x1 += 2;
					x2 += 2;
					for (BolasDeNieve p : SouthPark.mapas.get(1).tiros) {
						if(p.id == id) {
							p.x += 2;	
						}	
					}
				} else {
					x1++;
					x2++;
				}
				if (name.equals("Butters")) {
					currentImg = 1;
				}
			} 
			if (((Cartman.getCartman().y1 + ((Cartman.getCartman().y2 - Cartman.getCartman().y1) / 2))
					- (y1 + ((y2 - y1) / 2))) < 0) {
				if (SouthPark.pasarLvl2) {
					y1 -= 2;
					y2 -= 2;
					for (BolasDeNieve p : SouthPark.mapas.get(1).tiros) {
						if(p.id == id) {
							p.y -= 2;	
						}
					}
				} else {
					y1--;
					y2--;
				}
			} else if (((Cartman.getCartman().y1 + ((Cartman.getCartman().y2 - Cartman.getCartman().y1) / 2))
					- (y1 + ((y2 - y1) / 2))) > 0) {
				if (SouthPark.pasarLvl2) {
					y1 += 2;
					y2 += 2;
					for (BolasDeNieve p : SouthPark.mapas.get(1).tiros) {
						if(p.id == id) {
							p.y += 2;	
						}
					}
				} else {
					y1++;
					y2++;
				}
			}
		}
	}


	/**
	 * Generar disparo
	 */
	public BolasDeNieve shoot() {
		// TODO Auto-generated method stub
		if (cooldown == 75) {
			cooldown = 0;
			BolasDeNieve bolaNieve = new BolasDeNieve("BalaEnemigo", ((x1 + x2) / 2) - 20, ((y1 + y2) / 2) - 20,
					((x1 + x2) / 2) + 30, ((y1 + y2) / 2) + 30, "muneco.gif", direccion);
			return bolaNieve;
		} else {
			return null;
		}

	}

	/**
	 * Generar disparo des de arriba del enemigo
	 */
	public BolasDeNieve shoot1() {
		// TODO Auto-generated method stub
		if (cooldown == 75) {
			cooldown = 0;
			BolasDeNieve bolaNieve = new BolasDeNieve("BalaEnemigo", x1 - 20, y1 - 20, x1 + 30, y1 + 30, "muneco.gif",
					direccion);
			return bolaNieve;
		} else {
			return null;
		}

	}

	/**
	 * Generar disparo des de abajo del enemigo
	 */
	public BolasDeNieve shoot2() {
		// TODO Auto-generated method stub
		if (cooldown == 75) {
			cooldown = 0;
			BolasDeNieve bolaNieve = new BolasDeNieve("BalaEnemigo", x2 - 50, y2 - 50, x2, y2, "muneco.gif", direccion);
			return bolaNieve;
		} else {
			return null;
		}

	}

	/**
	 * Actualiza el cooldown de los tiros
	 */
	public void update() {
		if (cooldown < 75)
			cooldown++;
	}

	public void update2() {
		if (cooldown2 < 125)
			cooldown2++;
	}

	/**
	 * Mover Enemigo a la Izquierda cuando le golpean
	 */
	public void moveIzqDmg() {
		x1 -= 10;
		x2 -= 10;
		direccion = 'a';
	}

	/**
	 * Mover Enemigo a la Derecha cuando le golpean
	 */
	public void moveDerDmg() {
		// TODO Auto-generated method stub
		x1 += 10;
		x2 += 10;
		direccion = 'd';
	}

	/**
	 * Mover Enemigo Arriba cuando le golpean
	 */
	public void moveArrDmg() {
		y1 -= 10;
		y2 -= 10;
		direccion = 'w';

	}

	/**
	 * Mover Enemigo Abajo cuando le golpean
	 */
	public void moveAbaDmg() {
		y1 += 10;
		y2 += 10;
		direccion = 's';

	}

	

	public void heridoBalaEnemigo(Mapa mapa) {
		// TODO Auto-generated method stub
		for (BolasDeNieve t : mapa.tiros) {
			if (collidesWith(t)) {
				golpear();
			}
		}
	}


	/**
	 * Hace que los enemigos no choquen entre ellos, pero a veces se hacen los
	 * BAILONGOS
	 * 
	 * @param mapa
	 */
	public static void colisionesEnemigos(Mapa mapa) {
		// TODO Auto-generated method stub
		boolean colision = false;
		for (Enemigos e : mapa.enemigosList) {
			if (!e.collidesWithList(mapa.enemigosList).isEmpty()) {
				if (SouthPark.pasarLvl2) {
					if (!colision) {
						if (e.currentImg == 0) {
							e.x1 += 2;
							e.x2 += 2;
						} else {
							e.x1 -= 2;
							e.x2 -= 2;
						}
						colision = true;
					} else {
						if (e.currentImg == 0) {
							e.x1 -= 2;
							e.x2 -= 2;
						} else {
							e.x1 += 2;
							e.x2 += 2;
						}
					}
				} else {
					if (!colision) {
						if (e.currentImg == 0) {
							e.x1 -= 2;
							e.x2 -= 2;
						} else {
							e.x1 += 2;
							e.x2 += 2;
						}
						colision = true;
					} else {
						if (e.currentImg == 0) {
							e.x1 += 2;
							e.x2 += 2;
						} else {
							e.x1 -= 2;
							e.x2 -= 2;
						}
					}
				}
				

			}
		}
	}

	/**
	 * Genera los disparos de Garrison y dispara des de diferentes alturas
	 * aleatoriamente
	 * 
	 * @param mapa
	 */
	public void disparos(Field f, Mapa mapa) {
		// TODO Auto-generated method stub
		Random r = new Random();
		update();
		if (name.equals("Garrison")) {
			for (Sprite s : f.sprites) {
				if (s instanceof Cartman) {
					if ((x1 - s.x1) < 1000) {
						int tipoDeTiro = r.nextInt(3);
						if (tipoDeTiro == 0) {
							BolasDeNieve bola = (shoot());
							if (bola != null) {
								mapa.tiros.add(bola);

							}
						} else if (tipoDeTiro == 1) {
							BolasDeNieve bola2 = (shoot1());
							if (bola2 != null) {
								mapa.tiros.add(bola2);
							}

						} else {
							BolasDeNieve bola3 = (shoot2());
							if (bola3 != null) {
								mapa.tiros.add(bola3);
							}
						}

					}
				}
			}

		}
	}

	

	
	public BolasDeNieve shoot(Mapa mapa, int x1, int x2, int y1, int y2, int idl) {
		if (!delete) {
			int cx = (x1 + x2) / 2;
			int cy = (y1 + y2) / 2;
			if (cooldown2 == 125) {
				cooldown2 = 0;
				BolasDeNieve bolaNieve = new BolasDeNieve("BalaEnemigo", ((this.x1 + this.x2) / 2) - 10,
						((this.y1 + this.y2) / 2) - 10, ((this.x1 + this.x2) / 2) + 10, ((this.y1 + this.y2) / 2) + 10,
						cx, cy, idl, "patoRojo.png");

				mapa.tiros.add(bolaNieve);

				return bolaNieve;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	
}
