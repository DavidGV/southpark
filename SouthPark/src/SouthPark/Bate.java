package SouthPark;

import java.util.Iterator;

/**
 * Esta clase esta creada basicamente para el movimiento del bate
 * 
 * @author David Gil Vazquianez
 * @version 1.1
 *
 */
public class Bate extends Sprite{
	public Bate(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		// TODO Auto-generated constructor stub
		
	}
	char direccion='d';
	int cooldown=25; 
	/**
	 * Mover el bate a la Izquierda
	 */
	public void moveIzqB() {
		x1-=3; x2-=3;
		direccion='a';
	}
	/**
	 * Mover el bate a la Derecha
	 */
	public void moveDerB() {
		// TODO Auto-generated method stub
		x1+=3;x2+=3;
		direccion='d';
	}
	/**
	 * Mover el bate Arriba
	 */
	public void moveArrB() {
		y1-=3;y2-=3;
		direccion='w';
		
	}
	/**
	 * Mover el bate Abajo
	 */
	public void moveAbaB() {
		y1+=3;y2+=3;
		direccion='s';
		
	}
	/**
	 * Mover el bate a la Izquierda cuando golpean a Cartman
	 */
	public void moveIzqDmgB() {
		x1-=40; x2-=40;
		direccion='a';
	}
	/**
	 * Mover el bate a la Izquierda cuando golpean a Cartman
	 */
	public void moveDerDmgB() {
		// TODO Auto-generated method stub
		x1+=40; x2+=40;
		direccion='d';
	}
	/**
	 * Mover el bate a la Izquierda cuando golpean a Cartman
	 */
	public void moveArrDmgB() {
		y1-=40; y2-=40;
		direccion='w';
		
	}
	/**
	 * Mover el bate a la Izquierda cuando golpean a Cartman
	 */
	public void moveAbaDmgB() {
		y1+=40; y2+=40;
		direccion='s';
		
	}
	/**
	 * Actualizar cooldown de la direccion
	 */
	public void updateB() {
		if(cooldown<25)cooldown++;
	}
	/**
	 * Comprueba que el bate colisiona con los enemigos y si lo hace activa la
	 * funcion de bajar la vida a los enemigos y si han muerto los quita de la lista
	 * @param mapa 
	 */
	public void comprobarGolpe(Mapa mapa) {
		for (Enemigos e : mapa.enemigosList) {
			if (Cartman.getCartman().bate.collidesWith(e)) {
				e.golpear();
				if (e.currentImg == 0) {
					e.x1 += 40;
					e.x2 += 40;
				} else {
					e.x1 -= 40;
					e.x2 -= 40;
				}
			}

		}
		for (Iterator iterator = mapa.enemigosList.iterator(); iterator.hasNext();) {
			Enemigos s = (Enemigos) iterator.next();
			if (s.delete) {
				iterator.remove();
			}
		}
	}
	



}



