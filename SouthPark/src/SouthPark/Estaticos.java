package SouthPark;
/**
 * Esta clase esta creada basicamente para elementos estaticos
 * 
 * @author David Gil Vazquianez
 * @version 1.0
 *
 */
public class Estaticos extends Sprite{
	public Estaticos(String name, int x1, int y1, int x2, int y2, double angle, String[] path) {
		super(name, x1, y1, x2, y2, angle, path);
		// TODO Auto-generated constructor stub
		unscrollable=true;
		solid = false;
		cooldown = 0;
	}
	int cooldown;
	public void update() {
		if (cooldown<20) {
			cooldown++;
		}
	}
}
