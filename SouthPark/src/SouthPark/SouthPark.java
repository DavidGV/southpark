package SouthPark;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;



/**
 * Es un juego basado en la serie de South Park. Es un juego en el que te puedes
 * mover, pegar y esquivar a los enemigos Para moverte: 'w' moverte hacia
 * arriba, 's' moverte hacia abajo, 'a' moverte hacia la izquierda, 'd' moverte
 * hacia la derecha. Atacar: 'p'. Tambien puedes bloquear los proyectiles con el
 * bate presionando 'p' (haciendo un ataque).
 * 
 * @author David Gil Vazquianez
 * @version 1.0
 *
 */
public class SouthPark {

	// static ArrayList<Murciano> murcddddianos = new ArrayList<>();
	static Field f = new Field();
	static Window w = new Window(f);
	/**
	 * Poner el fondo
	 */
	static Terreno fondo1 = new Terreno("fondo", 0, 0, 5400, 1000, "fondo.png");
	static int score = 0;
	static Texto texto = new Texto("texto", 1600, 10, 1700, 25, "Score:" + score);
	/**
	 * Poner el fondo3
	 */
	// static Terreno fondo3 = new Terreno("fondo3", 0, 0, 1760, 8064, "mapa3.png");
	/**
	 * Poner el fondo3
	 */
	static Terreno fondo3 = new Terreno("fondo3", 0, 0, 1760, 13509, "mapa3-2.png");
	/**
	 * Sprites de bloques
	 */
	static String[] spritesBloques = { "pinchos.png" };
	/**
	 * Sprites de fondoG
	 */
	static String[] spritesGuardar = { "fondoGuardar.png" };
	/**
	 * Sprites de boton guardar
	 */
	static String[] spritesG = { "bGuardarD.png", "bGuardarA.png" };
	/**
	 *Sprites de boton cargar
	 */
	static String[] spritesC = { "bCargarD.png", "bCargarA.png" };
	/**
	 * Sprites de boton ranking
	 */
	static String[] spritesR = { "bRankingD.png", "bRankingA.png" };
	
	/**
	 * Sprites de Patos
	 */
	static String[] spritesPatos = { "pato.png" };
	/**
	 * Sprites vacios
	 */
	static String[] spritesVacios = { "" };

	/**
	 * Sprites de PatosRey
	 */
	static String[] spritesPatoRey = { "patoAzulRey.png" };
	/**
	 * Sprites de Butters
	 */
	static String[] spritesButters = { "buters.gif", "butersReves.gif" };
	/**
	 * Sprites de Garrison
	 */
	static String[] spritesGarrison = { "garrison.gif", "garrison.gif" };
	/**
	 * Sprites de Vidas
	 */
	static String[] spritesVidas = { "3corazones.png", "2corazones.png", "1corazones.png", "0corazones.png" };
	/**
	 * Sprites de la pantalla que sale cuando Ganas o Pierdes
	 */
	static String[] spritesGanarPerder = { "", "CuadorPerder.png", "CuadroGanar.png" };
	/**
	 * Sprites del menu
	 */
	static String[] spritesMenu = { "", "mapaMenu.png" };
	/**
	 * Sprites del jugar
	 */
	static String[] spritesJugar = { "bJugarD.png", "bJugarA.png" };
	/**
	 * Sprites del Instrucciones
	 */
	static String[] spritesInst = { "bInsD.png", "bInsA.png" };
	/**
	 * Sprites del salir
	 */
	static String[] spritesSalir = { "bSalirD.png", "bSalirA.png" };
	/**
	 * Sprites del N1
	 */
	static String[] spritesN1 = { "bNivel1D.png", "bNivel1A.png" };
	/**
	 * Sprites del N2
	 */
	static String[] spritesN2 = { "bNivel2D.png", "bNivel2A.png", "bNivel2N.png" };
	/**
	 * Sprites del N3
	 */
	static String[] spritesN3 = { "bNivel3D.png", "bNivel3A.png", "bNivel3N.png" };
	/**
	 * Sprites de las instrucciones
	 */
	static String[] spritesInstrucciones = { "", "instruc1.png", "instruc2.png", "instruc3.png" };
	/**
	 * Sprites de spritesWarningIzq
	 */
	static String[] spritesWarningIzq = { "", "warningIzq.png" };
	/**
	 * Sprites de spritesWarningDer
	 */
	static String[] spritesWarningDer = { "", "warningDer.png" };
	/**
	 * Crea el recuadro con las vidas
	 */
	static Estaticos corazones = new Estaticos("Corazones", 25, 25, 175, 75, 0, spritesVidas);
	/**
	 * Crea el menu
	 */
	static Estaticos menu = new Estaticos("menu", 0, 0, 1800, 1000, 0, spritesMenu);
	/**
	 * Crea el instrucciones
	 */
	static Estaticos instrucciones = new Estaticos("instrucciones", 0, 0, 1800, 1000, 0, spritesInstrucciones);
	/**
	 * Crea el recuadro de ganar o perder
	 */
	static Estaticos ganarPerder = new Estaticos("ganarPerder", 0, 400, 1764, 600, 0, spritesGanarPerder);
	/**
	 * Crea el recuadro warningIzq
	 */
	static Estaticos warningIzq = new Estaticos("warningIzq", 0, 0, 107, 960, 0, spritesWarningIzq);
	/**
	 * Crea el recuadro warningDer
	 */
	static Estaticos warningDer = new Estaticos("warningDer", 1649, 0, 1764, 960, 0, spritesWarningDer);
	// static Davilillo d = new Davilillo("Davilillo", 350, 300, 400, 350,
	// "davi.gif");
	/**
	 * Crear la lista de enemigoss
	 */
	// static ArrayList<Enemigos> enemigosList = new ArrayList<>();
	/**
	 * Crear la lista de terrenos
	 */
	// static ArrayList<Terreno> tierras = new ArrayList<>();; 
	/**
	 * Crear la lista de tiros
	 */
	// static ArrayList<BolasDeNieve> tiros = new ArrayList<>();
	/**
	 * Crear la lista de sprites
	 */
	// static ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	/**
	 * Vidas de Cartman
	 */
	// static int vida = 3;
	/**
	 * Boolean para cambiar la posicion del bate
	 */
	static boolean atkBate = false;
	/**
	 * Boolean para cambiar vuelve a su posicion normal
	 */
	static boolean bateReturns = false;
	/**
	 * Boolean para cambiar el cuando Cartman gira
	 */
	static boolean bateReves = false;
	/**
	 * Boolean para activar la musica del Boss
	 */
	static boolean boolMusica = false;
	/**
	 * Pixeles del eje X en que Cartman hace scroll hacia la Izquierda
	 */
	static int minScroll = 600;
	/**
	 * Pixeles del eje X en que Cartman hace scroll hacia la Derecha
	 */
	static int maxScroll = 1000;

	/**
	 * Pixeles del eje y en que Cartman hace scroll hacia la abajo
	 */
	static int maxScrollY = 400;
	/**
	 * Boolean para pasar al lvl2
	 */
	static boolean pasarLvl2 = false;
	/**
	 * Boolean para pasar al lvl2
	 */
	static boolean pasarLvl2menu = false;

	/**
	 * Boolean para pasar al lvl3
	 */
	static boolean pasarlvl3 = false;
	/**
	 * Boolean para pasar al lvl3
	 */
	static boolean pasarlvl3menu = false;
	/**
	 * Crear Mapa1
	 */
	static ArrayList<Mapa> mapas = new ArrayList<Mapa>();

	static int contEnemigos;

	static int cooldownWarnings = 0;

	static int nivel = 1;

	static boolean ganar3 = false;

	// static int cooldownLvl2 = 275;

	static boolean nacePatoRey = false;
	
	static boolean estoyLvl1 = false;
	
	static boolean estoyLvl2 = false;
	
	static boolean estoyLvl3 = false;
	
	static int x = f.getCurrentMouseX();
	
	static int y = f.getCurrentMouseY();
	
	static boolean cargar = false;
	
	static ArrayList<Puntuacion> puntuaciones = new ArrayList<Puntuacion>();

	/**
	 * Main del Juego
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		texto.font = new Font("SansSerif", Font.PLAIN, 25);
		boolean salir = false;
		int opcion = 1;
		int opcion2 = 1;
		mapas.add(new Mapa("mapa1"));
		mapas.add(new Mapa("mapa2"));
		mapas.add(new Mapa("mapa3"));

		while (!salir) {
			f.resetScroll();
			/* Mapa. */vaciarListas(mapas.get(0));
			System.out.println("1.Jugar 2.Instrucciones 3.Salir");
			switch (inputLobby(opcion)) {
			case 1:
				vaciarListas(mapas.get(0));
				f.resetScroll();
				switch (inputLobby2(opcion2)) {
				case 1:
					vaciarListas(mapas.get(0));
					f.resetScroll();
					level1();
					break;
				case 2:
					vaciarListas(mapas.get(1));
					f.resetScroll();
					level2();
					break;
				case 3:
					vaciarListas(mapas.get(2));
					f.resetScroll();
					level3();
					break;
				}
				Thread.sleep(500);
				break;
			case 2:
				/* Mapa. */vaciarListas(mapas.get(0));
				instrucciones(1);
				Thread.sleep(500);
				break;
			case 3:
				System.out.println("Salir");
				salir = true;
				System.exit(0);
			default:
				break;
			}
		}

	}

	private static void inputMenu() throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		if (w.getPressedKeys().contains('g')||w.getPressedKeys().contains('G')) {
			Thread.sleep(150);
			boolean intro = false;
			Estaticos fondoGuardar = new Estaticos("fondoGuardar", 0, 0, 1800, 1000, 0, spritesGuardar);
			fondoGuardar.useImgArray = true;
			fondoGuardar.currentImg = 0;
			Estaticos bN1g = new Estaticos("bN1g", 650, 250, 1095, 350,0, spritesG);//445-100
			Estaticos bN2g = new Estaticos("bN2g", 650, 450, 1095, 550,0, spritesC);
			Estaticos bN3g = new Estaticos("bN3g", 650, 650, 1095, 750,0, spritesR);
			while (!intro) {
				x = f.getCurrentMouseX();
				y = f.getCurrentMouseY();
				int xo = f.getMouseOverX();
				int yo = f.getMouseOverY();
				bN1g.useImgArray = true;
				bN1g.currentImg = 0;
				bN2g.useImgArray = true;
				bN2g.currentImg = 0;
				bN3g.useImgArray = true;
				bN3g.currentImg = 0;
				ArrayList<Sprite> sprites = new ArrayList<Sprite>();
				if (estoyLvl1) {
					sprites.add(fondo1);
					sprites.add(corazones);
					sprites.add(ganarPerder);
					sprites.add(Cartman.getCartman());
					sprites.addAll(mapas.get(0).enemigosList);
					sprites.add(Cartman.getCartman().bate);
					sprites.addAll(mapas.get(0).tierras);
					sprites.addAll(mapas.get(0).tiros);
				} else if (estoyLvl2) {
					sprites.add(warningIzq);
					sprites.add(warningDer);
					sprites.add(corazones);
					sprites.add(Cartman.getCartman());
					sprites.addAll(mapas.get(1).tierras);
					sprites.addAll(mapas.get(1).enemigosList);
					sprites.addAll(mapas.get(1).tiros);
					sprites.add(ganarPerder);
				} else if (estoyLvl3) {
					sprites.add(fondo3);
					sprites.add(corazones);
					sprites.add(Cartman.getCartman());
					sprites.add(ganarPerder);
					sprites.addAll(mapas.get(2).enemigosList);
					sprites.addAll(mapas.get(2).tierras);
					sprites.addAll(mapas.get(2).tiros);
				}
				sprites.add(fondoGuardar);
				sprites.add(texto);
				sprites.add(bN1g);
				sprites.add(bN2g);
				sprites.add(bN3g);
				f.draw(sprites);
				if (w.getPressedKeys().contains('g')||w.getPressedKeys().contains('G')) {
					intro = true;
				}
				for (Sprite s : sprites) {
					if (s.name.equals("bN1g") || s.name.equals("bN2g") || s.name.equals("bN3g")){
						if (s.collidesWithPoint(xo, yo)) {
							s.currentImg = 1;
						} else {
							s.currentImg = 0;
						}
					}

					if (s.collidesWithPoint(x, y)) {
						if (s.name.equals("bN1g")) {
							Guardar.guardar();
							Thread.sleep(150);
							intro = true;
						} else if (s.name.equals("bN2g")) {
							Guardar.cargar();
							cargar=true;
							Thread.sleep(150);
							intro = true;
						} else if (s.name.equals("bN3g"))
							Puntuacion.leerPuntuacion("resources/puntuaciones.csv");
							intro = true;
					}

				}
				Thread.sleep(150);
			}

		}
	}

	private static int inputLobby2(int opcion) throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		f.background = "mapaMenu.png";
		boolean intro = false;
		// char enter = (char)13;
		while (!intro) {
			texto = new Texto("texto", 1600, 10, 1700, 25, "Score:" + score);
			texto.font = new Font("SansSerif", Font.PLAIN, 25);// Headliner No. 45
			// TODO Auto-generated method stub
			x = f.getCurrentMouseX();
			y = f.getCurrentMouseY();
			if (cargar) {
				Guardar.cargar2();	
			}
			int xo = f.getMouseOverX();
			int yo = f.getMouseOverY();
			inputMenu();
			Terreno bN1 = new Terreno("bN1", 1040, 250, 1485, 350, spritesN1);
			Terreno bN2 = new Terreno("bN2", 1040, 450, 1485, 550, spritesN2);
			Terreno bN3 = new Terreno("bN3", 1040, 650, 1485, 750, spritesN3);
			bN1.useImgArray = true;
			bN1.currentImg = 0;
			if (pasarLvl2menu) {
				bN2.useImgArray = true;
				bN2.currentImg = 0;
			} else {
				bN2.useImgArray = true;
				bN2.currentImg = 2;
			}
			if (pasarlvl3menu) {
				bN3.useImgArray = true;
				bN3.currentImg = 0;
			} else {
				bN3.useImgArray = true;
				bN3.currentImg = 2;
			}
			// System.out.println("x:" + x + " y:" + y + " xo:" + xo + " yo:" + yo);
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(texto);
			sprites.add(bN1);
			sprites.add(bN2);
			sprites.add(bN3);

			f.draw(sprites);
			for (Sprite s : sprites) {
				if (s.currentImg == 2) {

				} else {
					if (s.collidesWithPoint(xo, yo)) {
						s.currentImg = 1;
					} else {
						s.currentImg = 0;
					}
					if (s.collidesWithPoint(x, y)) {
						if (s.name.equals("bN1")) {
							opcion = 1;
							pasarLvl2 = false;
							pasarlvl3 = false;
							intro = true;
						} else if (s.name.equals("bN2")) {
							opcion = 2;
							pasarLvl2 = true;
							pasarlvl3 = false;
							intro = true;
						} else if (s.name.equals("bN3")) {
							opcion = 3;
							pasarLvl2 = false;
							pasarlvl3 = true;
							intro = true;
						}

					}
				}

			}
			Thread.sleep(150);
		}
		return opcion;

	}

	private static void level3() throws InterruptedException, IOException, ClassNotFoundException {
		Cartman c3 = Cartman.getCartman();
		init3();
		Thread.sleep(1000);
		estoyLvl3 = true;
		if (cargar) {
			Guardar.cargar2();	
		}
		boolean flag = false;
		while (!flag) {
			texto = new Texto("texto", 1600, 10, 1700, 25, "Score:" + score);
			texto.font = new Font("SansSerif", Font.PLAIN, 25);
			c3.update();
			inputMenu();
			input();
			if (c3.y1 < 12600) {// 12715
				f.scroll(0, -20);
				maxScrollY -= 20;
			}
			c3.y1 += 20;
			c3.y2 += 20;
			// c3.herido(mapas.get(2));
			c3.chocar(mapas.get(2));
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(fondo3);
			sprites.add(corazones);
			sprites.add(c3);
			sprites.add(ganarPerder);
			sprites.addAll(mapas.get(2).enemigosList);
			sprites.addAll(mapas.get(2).tierras);
			sprites.addAll(mapas.get(2).tiros);
			sprites.add(texto);
			f.draw(sprites);
			Thread.sleep(20);
			System.out.println("x1 " + c3.x1 + ",x2 " + c3.x2 + "y1 " + c3.y1 + ",y2 " + c3.y2);
			c3.marcadorVidas();
			if (c3.hp <= 0) {
				corazones.currentImg = 3;
				ganarPerder.currentImg = 1;
				// c.delete();
				// c.bate.delete();
				w.playMusic("PalLobby.wav");
				f.draw(sprites);
				Thread.sleep(500);
				w.stopMusic();
				flag = true;
				System.out.println("pal lobby");
				Date d = new Date();
				String nom = w.showInputPopup("Escribe tu nombre");
				String f = d+"";
				Puntuacion p = new Puntuacion (score, nom , f);
				puntuaciones.add(p);
				Puntuacion.guardarPuntuacion("resources/puntuaciones.csv");
				score=0;
			}
			if (ganar3()) {
				System.out.println("davilillo gana un poquillo");
				ganarPerder.currentImg = 2;
				w.playMusic("LetsGo.wav");
				f.draw(sprites);
				Thread.sleep(500);
				w.stopMusic();
				flag = true;
				System.out.println("Let's goooo");
				Date d = new Date();
				String nom = w.showInputPopup("Escribe tu nombre");
				String f = d+"";
				Puntuacion p = new Puntuacion (score, nom , f);
				puntuaciones.add(p);
				Puntuacion.guardarPuntuacion("resources/puntuaciones.csv");
				score=0;
			}

		}
		estoyLvl3 = false;
		boolean lvl4 = false;
		while (!lvl4) {
			if (w.getPressedKeys().contains('\n')) {
				lvl4 = true;
			}
		}

	}

	private static boolean ganar3() {
		// TODO Auto-generated method stub
		if (ganar3) {
			return true;
		}
		return false;
	}

	private static void init3() throws ClassNotFoundException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		Cartman c3 = Cartman.getCartman();
		ganar3 = false;
		c3.nivel(3);
		corazones = new Estaticos("Corazones", 25, 25, 175, 75, 0, spritesVidas);
		ganarPerder = new Estaticos("ganarPerder", 0, 400, 1764, 600, 0, spritesGanarPerder);
		fondo3.solid = false;
		generarBloques();
		ganarPerder.useImgArray = true;
		ganarPerder.currentImg = 0;
		corazones.useImgArray = true;
		corazones.currentImg = 0;
		c3.useImgArray = true;
		c3.currentImg = 0;
		minScroll = 600;
		maxScroll = 1000;
	}

	private static void generarBloques() {
		// TODO Auto-generated method stub
		Enemigos win = new Enemigos("win", 700, 13235, 1078, 13505, 0, spritesVacios);
		Enemigos lose1 = new Enemigos("bloque", 405, 13315, 699, 13505, 0, spritesVacios);
		Enemigos lose2 = new Enemigos("bloque", 1079, 13315, 1350, 13505, 0, spritesVacios);
		Enemigos b1 = new Enemigos("bloque", 740, 1050, 990, 1150, 0, spritesBloques);
		Enemigos b2 = new Enemigos("bloque", 405, 2000, 655, 2100, 0, spritesBloques);
		Enemigos b3 = new Enemigos("bloque", 1100, 2000, 1350, 2100, 0, spritesBloques);
		Enemigos b4 = new Enemigos("bloque", 740, 3000, 990, 3100, 0, spritesBloques);
		Enemigos b5 = new Enemigos("bloque", 405, 4000, 655, 4100, 0, spritesBloques);
		Enemigos b6 = new Enemigos("bloque", 1100, 4000, 1350, 4100, 0, spritesBloques);
		Enemigos b7 = new Enemigos("bloque", 585, 5000, 835, 5100, 0, spritesBloques);
		Enemigos b8 = new Enemigos("bloque", 915, 5500, 1165, 5600, 0, spritesBloques);
		Enemigos b9 = new Enemigos("bloque", 405, 6500, 655, 6600, 0, spritesBloques);
		Enemigos b10 = new Enemigos("bloque", 1100, 6500, 1350, 6600, 0, spritesBloques);
		Enemigos b11 = new Enemigos("bloque", 740, 7500, 990, 7600, 0, spritesBloques);
		Enemigos b12 = new Enemigos("bloque", 740, 8000, 990, 8100, 0, spritesBloques);
		Enemigos b13 = new Enemigos("bloque", 915, 9000, 1165, 9100, 0, spritesBloques);
		Enemigos b14 = new Enemigos("bloque", 585, 9500, 835, 9600, 0, spritesBloques);
		Enemigos b15 = new Enemigos("bloque", 405, 10500, 655, 10600, 0, spritesBloques);
		Enemigos b16 = new Enemigos("bloque", 1100, 11000, 1350, 11100, 0, spritesBloques);
		Enemigos b17 = new Enemigos("bloque", 740, 12000, 990, 12100, 0, spritesBloques);
		mapas.get(2).enemigosList.add(win);
		mapas.get(2).enemigosList.add(lose1);
		mapas.get(2).enemigosList.add(lose2);
		mapas.get(2).enemigosList.add(b1);
		mapas.get(2).enemigosList.add(b2);
		mapas.get(2).enemigosList.add(b3);
		mapas.get(2).enemigosList.add(b4);
		mapas.get(2).enemigosList.add(b5);
		mapas.get(2).enemigosList.add(b6);
		mapas.get(2).enemigosList.add(b7);
		mapas.get(2).enemigosList.add(b8);
		mapas.get(2).enemigosList.add(b9);
		mapas.get(2).enemigosList.add(b10);
		mapas.get(2).enemigosList.add(b11);
		mapas.get(2).enemigosList.add(b12);
		mapas.get(2).enemigosList.add(b13);
		mapas.get(2).enemigosList.add(b14);
		mapas.get(2).enemigosList.add(b15);
		mapas.get(2).enemigosList.add(b16);
		mapas.get(2).enemigosList.add(b17);
		win.useImgArray = true;
		win.currentImg = 0;
		lose1.useImgArray = true;
		lose1.currentImg = 0;
		lose2.useImgArray = true;
		lose2.currentImg = 0;
		b1.useImgArray = true;
		b1.currentImg = 0;
		b2.useImgArray = true;
		b2.currentImg = 0;
		b3.useImgArray = true;
		b3.currentImg = 0;
		b4.useImgArray = true;
		b4.currentImg = 0;
		b5.useImgArray = true;
		b5.currentImg = 0;
		b6.useImgArray = true;
		b6.currentImg = 0;
		b7.useImgArray = true;
		b7.currentImg = 0;
		b8.useImgArray = true;
		b8.currentImg = 0;
		b9.useImgArray = true;
		b9.currentImg = 0;
		b10.useImgArray = true;
		b10.currentImg = 0;
		b11.useImgArray = true;
		b11.currentImg = 0;
		b12.useImgArray = true;
		b12.currentImg = 0;
		b13.useImgArray = true;
		b13.currentImg = 0;
		b14.useImgArray = true;
		b14.currentImg = 0;
		b15.useImgArray = true;
		b15.currentImg = 0;
		b16.useImgArray = true;
		b16.currentImg = 0;
		b17.useImgArray = true;
		b17.currentImg = 0;
	}

	private static void level2() throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Cartman c2 = Cartman.getCartman();
		init2();
		if (cargar) {
			Guardar.cargar3();	
		}
		estoyLvl2 = true;
		boolean ene0 = false;
		boolean ene1 = false;
		boolean ene2 = false;
		boolean ene3 = false;
		boolean flag = false;
		nacePatoRey = false;
		boolean generarEnemigosRey = false;
		while (!flag) {
			texto = new Texto("texto", 1600, 10, 1700, 25, "Score:" + score);
			texto.font = new Font("SansSerif", Font.PLAIN, 25);
			c2.update();
			inputMenu();
			int x = f.getMouseOverX();
			int y = f.getMouseOverY();
			c2.shoot(mapas.get(1), x, y);
			c2.cambiarSpritesLvl2(x, y);

			for (Enemigos e : mapas.get(1).enemigosList) {
				if (e.name.equals("patoRey")) {
					e.updatePatoRey();
					if (e.cooldownPatoRey == 300) {
						generarEnemigosRey = true;
						System.out.println("generarEnemigos");
					}
				} else {
					e.perseguir();
					e.shoot(mapas.get(1), c2.x1, c2.x2, c2.y1, c2.y2, e.id);
					e.update2();
					for (BolasDeNieve p : mapas.get(1).tiros) {
						if (p.name.equals("BalaEnemigo") && p.id == e.id) {
							// System.out.println();
							p.moveBalasLvl2(e.x1, e.x2, e.y1, e.y2, e.id);
						}
					}
				}

			}
			if (generarEnemigosRey) {
				generarEnemigos24();
				generarEnemigosRey = false;
			}
			for (BolasDeNieve p : mapas.get(1).tiros) {
				// p.moveBalasLvl2(e.x1, e.x2, e.y1, e.y2);
				if (p.name.equals("BalaCartman")) {
					p.moveBalasLvl2(c2.x1, c2.x2, c2.y1, c2.y2, 0);
				}
				p.update2();
				p.colisionesTiros(mapas.get(1));
			}
			input();
			c2.herido2(mapas.get(1));
			c2.marcadorVidas();
			Enemigos.colisionesEnemigos(mapas.get(1));

			musica();
			for (Iterator iterator = mapas.get(1).tiros.iterator(); iterator.hasNext();) {
				BolasDeNieve s = (BolasDeNieve) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			for (Iterator iterator2 = mapas.get(1).enemigosList.iterator(); iterator2.hasNext();) {
				Enemigos e = (Enemigos) iterator2.next();
				if (e.delete && e.name.equals("patoRey")) {
					iterator2.remove();
				}
			}

			if (contEnemigos == 0 && !ene0) {
				// System.out.println("entro al while");
				warningIzq.update();
				// System.out.println(warningDer.cooldown);
				if (warningIzq.cooldown == 20) {
					// System.out.println("entro en el if");
					if (warningIzq.currentImg == 1) {
						warningIzq.currentImg = 0;
						// System.out.println("cambio a 0");
					} else {
						warningIzq.currentImg = 1;
						// System.out.println("cambio a 1");
					}
					cooldownWarnings++;
					warningIzq.cooldown = 0;
				}
				if (cooldownWarnings == 6) {
					generarEnemigos2();
					ene0 = true;
				}

			}
			if (contEnemigos >= 3 && !ene1) {
				// System.out.println("entro al while");
				warningDer.update();
				// System.out.println(warningDer.cooldown);
				if (warningDer.cooldown == 20) {
					// System.out.println("entro en el if");
					if (warningDer.currentImg == 1) {
						warningDer.currentImg = 0;
						// System.out.println("cambio a 0");
					} else {
						warningDer.currentImg = 1;
						// System.out.println("cambio a 1");
					}
					cooldownWarnings++;
					warningDer.cooldown = 0;
				}
				if (cooldownWarnings == 12) {
					generarEnemigos21();
					ene1 = true;
				}

			}
			if (contEnemigos >= 6 && !ene2) {
				// System.out.println("entro al while");
				warningDer.update();
				// System.out.println(warningDer.cooldown);
				if (warningDer.cooldown == 20) {
					// System.out.println("entro en el if");
					if (warningDer.currentImg == 1) {
						warningDer.currentImg = 0;
						warningIzq.currentImg = 0;
						// System.out.println("cambio a 0");
					} else {
						warningDer.currentImg = 1;
						warningIzq.currentImg = 1;
						// System.out.println("cambio a 1");
					}
					cooldownWarnings++;
					warningDer.cooldown = 0;
				}
				if (cooldownWarnings == 18) {
					generarEnemigos22();
					ene2 = true;
				}

			}
			if (contEnemigos >= 12 && !ene3) {
				// System.out.println("entro al while");
				warningDer.update();
				// System.out.println(warningDer.cooldown);
				if (warningDer.cooldown == 20) {
					// System.out.println("entro en el if");
					if (warningDer.currentImg == 1) {
						warningDer.currentImg = 0;
						// System.out.println("cambio a 0");
					} else {
						warningDer.currentImg = 1;
						// System.out.println("cambio a 1");
					}
					cooldownWarnings++;
					warningDer.cooldown = 0;
				}
				if (cooldownWarnings == 24) {
					generarEnemigos23();
					ene3 = true;
				}

			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			// sprites.add(fondo1);
			sprites.add(warningIzq);
			sprites.add(warningDer);
			sprites.add(corazones);
			sprites.add(c2);
			// sprites.addAll(mapas.get(1).enemigosList);
			// sprites.add(c2.bate);
			sprites.addAll(mapas.get(1).tierras);
			sprites.addAll(mapas.get(1).enemigosList);
			sprites.addAll(mapas.get(1).tiros);
			sprites.add(ganarPerder);
			sprites.add(texto);
			f.draw(sprites);
			Thread.sleep(20);
			if (c2.hp <= 0) {
				corazones.currentImg = 3;
				ganarPerder.currentImg = 1;
				w.playMusic("PalLobby.wav");
				f.draw();
				Thread.sleep(500);
				w.stopMusic();
				flag = true;
				System.out.println("pal lobby");
				// f.draw();
				Date d = new Date();
				String nom = w.showInputPopup("Escribe tu nombre");
				String f = d+"";
				Puntuacion p = new Puntuacion (score, nom , f);
				puntuaciones.add(p);
				Puntuacion.guardarPuntuacion("resources/puntuaciones.csv");
				score=0;
			}
			if (ganar2()) {
				System.out.println("davilillo gana un poquillo2");
				ganarPerder.currentImg = 2;
				w.playMusic("LetsGo.wav");
				f.draw();
				Thread.sleep(500);
				w.stopMusic();
				flag = true;
				pasarlvl3menu = true;
				System.out.println("Let's goooo");
			}
			// f.draw();
		}
		estoyLvl2 = false;
		boolean lvl3 = false;
		while (!lvl3) {
			if (w.getPressedKeys().contains('\n')) {
				lvl3 = true;
			}
		}

	}

	private static void generarEnemigos24() {
		// TODO Auto-generated method stub
		Enemigos p13 = new Enemigos("pato", 1250, 400, 0, spritesPatos);
		Enemigos p14 = new Enemigos("pato", 1250, 700, 0, spritesPatos);
		mapas.get(1).enemigosList.add(p13);
		mapas.get(1).enemigosList.add(p14);
		p13.useImgArray = true;
		p13.currentImg = 0;
		p14.useImgArray = true;
		p14.currentImg = 0;
		for (Enemigos e : mapas.get(1).enemigosList) {
			if (e.name.equals("patoRey")) {
				e.cooldownPatoRey = 0;
			}
		}
	}

	private static void generarEnemigos23() {
		// TODO Auto-generated method stub
		Enemigos patoRey = new Enemigos("patoRey", 1305, 50, 1690, 925, 0, spritesPatoRey);
		nacePatoRey = true;
		mapas.get(1).enemigosList.add(patoRey);
		patoRey.useImgArray = true;
		patoRey.currentImg = 0;
	}

	private static void generarEnemigos22() {
		// TODO Auto-generated method stub
		Enemigos p7 = new Enemigos("pato", 1570, 300, 0, spritesPatos);
		Enemigos p8 = new Enemigos("pato", 1570, 500, 0, spritesPatos);
		Enemigos p9 = new Enemigos("pato", 1570, 700, 0, spritesPatos);
		Enemigos p10 = new Enemigos("pato", 20, 300, 0, spritesPatos);
		Enemigos p11 = new Enemigos("pato", 20, 500, 0, spritesPatos);
		Enemigos p12 = new Enemigos("pato", 20, 700, 0, spritesPatos);
		mapas.get(1).enemigosList.add(p7);
		mapas.get(1).enemigosList.add(p8);
		mapas.get(1).enemigosList.add(p9);
		mapas.get(1).enemigosList.add(p10);
		mapas.get(1).enemigosList.add(p11);
		mapas.get(1).enemigosList.add(p12);
		p7.useImgArray = true;
		p7.currentImg = 0;
		p8.useImgArray = true;
		p8.currentImg = 0;
		p9.useImgArray = true;
		p9.currentImg = 0;
		p10.useImgArray = true;
		p10.currentImg = 0;
		p11.useImgArray = true;
		p11.currentImg = 0;
		p12.useImgArray = true;
		p12.currentImg = 0;
	}

	private static void generarEnemigos21() {
		// TODO Auto-generated method stub
		Enemigos p4 = new Enemigos("pato", 1570, 300, 0, spritesPatos);
		Enemigos p5 = new Enemigos("pato", 1570, 500, 0, spritesPatos);
		Enemigos p6 = new Enemigos("pato", 1570, 700, 0, spritesPatos);
		mapas.get(1).enemigosList.add(p4);
		mapas.get(1).enemigosList.add(p5);
		mapas.get(1).enemigosList.add(p6);
		p4.useImgArray = true;
		p4.currentImg = 0;
		p5.useImgArray = true;
		p5.currentImg = 0;
		p6.useImgArray = true;
		p6.currentImg = 0;
	}

	private static void init2() throws ClassNotFoundException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		Cartman c2 = Cartman.getCartman();
		corazones = new Estaticos("Corazones", 25, 25, 175, 75, 0, spritesVidas);
		ganarPerder = new Estaticos("ganarPerder", 0, 400, 1764, 600, 0, spritesGanarPerder);
		f.background = "espacio.jpg";
		// generarEnemigos2();
		c2.nivel(2);
		contEnemigos = 0;
		cooldownWarnings = 0;
		ganarPerder.useImgArray = true;
		ganarPerder.currentImg = 0;
		corazones.useImgArray = true;
		corazones.currentImg = 0;
		warningIzq.useImgArray = true;
		warningIzq.currentImg = 0;
		warningDer.useImgArray = true;
		warningDer.currentImg = 0;
		c2.useImgArray = true;
		c2.currentImg = 0;
	}

	/**
	 * Pantalla de instrucciones
	 */
	private static void instrucciones(int num) throws InterruptedException {
		// TODO Auto-generated method stub
		instrucciones.useImgArray = true;
		instrucciones.currentImg = 1;
		boolean intro = false;
		while (!intro) {
			if (w.getPressedKeys().contains('\n')) {
				intro = true;
			} else if (w.getPressedKeys().contains('d')&& instrucciones.currentImg == 1) {
				instrucciones.currentImg = 2;
			} else if (w.getPressedKeys().contains('d')&& instrucciones.currentImg == 2) {
				instrucciones.currentImg = 3;
			} else if (w.getPressedKeys().contains('d')&& instrucciones.currentImg == 3) {
				instrucciones.currentImg = 1;
			} else if (w.getPressedKeys().contains('a')&& instrucciones.currentImg == 1) {
				instrucciones.currentImg = 3;
			} else if (w.getPressedKeys().contains('a')&& instrucciones.currentImg == 2) {
				instrucciones.currentImg = 1;
			} else if (w.getPressedKeys().contains('a')&& instrucciones.currentImg == 3) {
				instrucciones.currentImg = 2;
			}
			
			mapas.get(0).sprites.add(instrucciones);
			f.draw(mapas.get(0).sprites);
			Thread.sleep(150);
		}
	}

	/**
	 * Vacia todas las listas
	 */
	private static void vaciarListas(Mapa mapa) {
		// TODO Auto-generated method stub
		mapa.enemigosList = new ArrayList<>();
		mapa.tierras = new ArrayList<>();
		mapa.tiros = new ArrayList<>();
		mapa.sprites = new ArrayList<Sprite>();
	}

	/**
	 * Movimientos en el lobby + interaccion
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	private static int inputLobby(int opcion) throws InterruptedException, IOException, ClassNotFoundException {
		f.background = "mapaMenu.png";
		boolean intro = false;
		Terreno bJugar = new Terreno("bJugar", 1040, 250, 1485, 350, spritesJugar);
		Terreno bInstrucciones = new Terreno("bInstrucciones", 1040, 450, 1485, 550, spritesInst);
		Terreno bSalir = new Terreno("bSalir", 1040, 650, 1485, 750, spritesSalir);
		bJugar.useImgArray = true;
		bInstrucciones.useImgArray = true;
		bSalir.useImgArray = true;
		// char enter = (char)13;
		while (!intro) {
			// TODO Auto-generated method stub
			texto = new Texto("texto", 1600, 10, 1700, 25, "Score:" + score);
			texto.font = new Font("SansSerif", Font.PLAIN, 25);
			x = f.getCurrentMouseX();
			y = f.getCurrentMouseY();
			int xo = f.getMouseOverX();
			int yo = f.getMouseOverY();
			bJugar.currentImg = 0;
			bInstrucciones.currentImg = 0;
			bSalir.currentImg = 0;
			inputMenu();
			// System.out.println("x:" + x + " y:" + y + " xo:" + xo + " yo:" + yo);
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(texto);
			sprites.add(bJugar);
			sprites.add(bInstrucciones);
			sprites.add(bSalir);
			f.draw(sprites);
			for (Sprite s : sprites) {
				if (s.name.equals("menu")) {

				} else {
					if (s.collidesWithPoint(xo, yo)) {
						s.currentImg = 1;
					} else {
						s.currentImg = 0;
					}
				}

				if (s.collidesWithPoint(x, y)) {
					if (s.name.equals("bJugar")) {
						opcion = 1;
						intro = true;
					} else if (s.name.equals("bInstrucciones")) {
						opcion = 2;
						intro = true;
					} else if (s.name.equals("bSalir")) {
						opcion = 3;
						intro = true;
					}

				}
			}
			Thread.sleep(150);
		}
		return opcion;
	}

	/**
	 * Todo el nivel 1
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	private static void level1() throws InterruptedException, IOException, ClassNotFoundException {
		Cartman c = Cartman.getCartman();
		init1();
		if (cargar) {
			System.out.println("cargar3");
			Guardar.cargar3();	
		}
		estoyLvl1 = true;
		boolean flag = false;
		while (!flag) {
			texto = new Texto("texto", 1600, 10, 1700, 25, "Score:" + score);
			texto.font = new Font("SansSerif", Font.PLAIN, 25);
			c.update();
			inputMenu();
			//System.out.println("x1:"+c.x1+"x2:"+c.x2);
			// bate.update();
			input();
			if (c.currentImg == 0) {
				c.atacar();
			} else {
				c.atacarReves();
			}
			c.herido(mapas.get(0));
			batePegar();
			c.bate.comprobarGolpe(mapas.get(0));
			for (Enemigos e : mapas.get(0).enemigosList) {
				e.perseguir();
				e.disparos(f, mapas.get(0));
			}
			Enemigos.colisionesEnemigos(mapas.get(0));
			musica();
			for (BolasDeNieve p : mapas.get(0).tiros) {
				p.move();
				p.colisionesTiros(mapas.get(0));

			}
			for (Iterator iterator = mapas.get(0).tiros.iterator(); iterator.hasNext();) {
				BolasDeNieve s = (BolasDeNieve) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(fondo1);
			sprites.add(corazones);
			sprites.add(ganarPerder);
			sprites.add(c);
			sprites.addAll(mapas.get(0).enemigosList);
			sprites.add(c.bate);
			sprites.addAll(mapas.get(0).tierras);
			sprites.addAll(mapas.get(0).tiros);
			sprites.add(texto);
			f.draw(sprites);
			Thread.sleep(20);
			// System.out.println("x1 "+c.x1+",y1 "+c.y1+",x2 "+c.x2+",y2 "+c.y2);
			c.marcadorVidas();
			if (c.hp <= 0) {
				corazones.currentImg = 3;
				ganarPerder.currentImg = 1;
				// c.delete();
				// c.bate.delete();
				w.playMusic("PalLobby.wav");
				f.draw(sprites);
				Thread.sleep(500);
				w.stopMusic();
				flag = true;
				System.out.println("pal lobby");
				Date d = new Date();
				String nom = w.showInputPopup("Escribe tu nombre");
				String f = d+"";
				Puntuacion p = new Puntuacion (score, nom , f);
				puntuaciones.add(p);
				Puntuacion.guardarPuntuacion("resources/puntuaciones.csv");
				score=0;
			}
			if (ganar()) {
				System.out.println("davilillo gana un poquillo");
				ganarPerder.currentImg = 2;
				w.playMusic("LetsGo.wav");
				f.draw(sprites);
				Thread.sleep(500);
				w.stopMusic();
				flag = true;
				pasarLvl2menu = true;
				System.out.println("Let's goooo");
			}

		}
		estoyLvl1 = false;
		boolean lvl2 = false;
		while (!lvl2) {
			if (w.getPressedKeys().contains('\n')) {
				lvl2 = true;
			}
		}

	}

	private static void init1() throws ClassNotFoundException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		Cartman c = Cartman.getCartman();
		c.nivel(1);
		corazones = new Estaticos("Corazones", 25, 25, 175, 75, 0, spritesVidas);
		ganarPerder = new Estaticos("ganarPerder", 0, 400, 1764, 600, 0, spritesGanarPerder);
		// Field.resetScroll();
		w.playMusic("CancionNivel.wav");
		fondo1.solid = false;
		generarEnemigos();
		atkBate = false;
		bateReturns = false;
		bateReves = false;
		boolMusica = false;
		ganarPerder.useImgArray = true;
		ganarPerder.currentImg = 0;
		corazones.useImgArray = true;
		corazones.currentImg = 0;
		c.useImgArray = true;
		c.currentImg = 0;
		minScroll = 600;
		maxScroll = 1000;
	}

	private static void batePegar() {
		// XXX eso no tendria que estar en input?
		// TODO Auto-generated method stub
		if (Cartman.getCartman().currentImg == 0 && (w.getPressedKeys().contains('p') || w.getPressedKeys().contains('P'))) {
			if (w.getPressedKeys().contains('a')) {
				atkBate = true;
				bateReves = true;
			}
		} else if (Cartman.getCartman().currentImg == 1 && (w.getPressedKeys().contains('p') || w.getPressedKeys().contains('P'))) {
			if (w.getPressedKeys().contains('d')) {
				atkBate = false;
				bateReves = false;
			}
		}
	}
	/**
	 * Confirma que has ganado
	 */
	private static boolean ganar() {
		// TODO Auto-generated method stub
		for (Enemigos e : mapas.get(0).enemigosList) {
			if (e.name.equals("Garrison")) {
				return false;
			}
		}
		return true;
	}

	private static boolean ganar2() {
		// TODO Auto-generated method stub
		if (contEnemigos >= 12 && nacePatoRey) {
			for (Enemigos e : mapas.get(1).enemigosList) {
				if (e.name.equals("patoRey")) {
					System.out.println("Rey vivo");
					return false;
				}
			}
			System.out.println("Ganamos");
			return true;
		}
		return false;
	}

	/**
	 * Activa la musica del Boss
	 */
	private static void musica() {
		// TODO Auto-generated method stub
		if (Cartman.getCartman().x1 >= 3480 && !boolMusica) {
			w.playMusic("AudioBoss.wav");
			boolMusica = true;
		}
	}
	/**
	 * Genera a los enemigos y los mete a la lista de enemigos
	 */
	private static void generarEnemigos() {
		// TODO Auto-generated method stub
		Enemigos b = new Enemigos("Butters", 1025, 775, spritesButters);
		Enemigos b2 = new Enemigos("Butters", 900, 735, spritesButters);
		Enemigos b3 = new Enemigos("Butters", 925, 900, spritesButters);
		Enemigos b4 = new Enemigos("Butters", 3050, 900, spritesButters);
		Enemigos b5 = new Enemigos("Butters", -100, 900, spritesButters);
		Enemigos b6 = new Enemigos("Butters", -150, 850, spritesButters);
		Enemigos b9 = new Enemigos("Butters", 2800, 900, spritesButters);
		Enemigos g = new Enemigos("Garrison", 4480, 750, 4580, 950, 0, spritesGarrison);
		mapas.get(0).enemigosList.add(b);
		mapas.get(0).enemigosList.add(b2);
		mapas.get(0).enemigosList.add(b3);
		mapas.get(0).enemigosList.add(b4);
		mapas.get(0).enemigosList.add(b5);
		mapas.get(0).enemigosList.add(b6);
		mapas.get(0).enemigosList.add(b9);
		mapas.get(0).enemigosList.add(g);
		b.useImgArray = true;
		b.currentImg = 0;
		b2.useImgArray = true;
		b2.currentImg = 0;
		b3.useImgArray = true;
		b3.currentImg = 0;
		b4.useImgArray = true;
		b4.currentImg = 0;
		b5.useImgArray = true;
		b5.currentImg = 0;
		b6.useImgArray = true;
		b6.currentImg = 0;
		b9.useImgArray = true;
		b9.currentImg = 0;
		g.useImgArray = true;
		g.currentImg = 0;

	}

	private static void generarEnemigos2() {
		// TODO Auto-generated method stub
		Enemigos p = new Enemigos("pato", 10, 500, 0, spritesPatos);
		Enemigos p2 = new Enemigos("pato", 20, 300, 0, spritesPatos);
		Enemigos p3 = new Enemigos("pato", 15, 700, 0, spritesPatos);
		mapas.get(1).enemigosList.add(p);
		mapas.get(1).enemigosList.add(p2);
		mapas.get(1).enemigosList.add(p3);
		p.useImgArray = true;
		p.currentImg = 0;
		p2.useImgArray = true;
		p2.currentImg = 0;
		p3.useImgArray = true;
		p3.currentImg = 0;

	}

	/**
	 * Hace que te puedas mover en todas direcciones, pone el scroll y tambien mueve
	 * el bate
	 */
	private static void input() {
		// TODO Auto-generated method stub
		if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('A')) { // && !(w.getPressedKeys().contains('p'))) {
			if (pasarLvl2) {
				if (Cartman.getCartman().x1 > 0) {
					Cartman.getCartman().moveIzq();
					for (BolasDeNieve p : mapas.get(1).tiros) {
						if (p.name.equals("BalaCartman")) {
							p.x -= 3;
						}

					}
				}
			} else if (pasarlvl3) {
				if (Cartman.getCartman().x1 > 411) {
					Cartman.getCartman().moveIzqPlus();
				}

			} else {
				if (Cartman.getCartman().x1 > 0) {
					if (Cartman.getCartman().x1 > 600) {
						if (Cartman.getCartman().x1 < minScroll) {
							// scroll = true;
							// scroll positiu: A la dreta.
							f.scroll(3, 0);
							// has d'ajustar minscroll i maxscroll de la mateixa forma
							minScroll -= 3;
							maxScroll -= 3;
						}
					}

					Cartman.getCartman().moveIzq();
					Cartman.getCartman().bate.moveIzqB();

					if (!bateReves) {
						Cartman.getCartman().currentImg = 1;
						bateReves = true;
						Cartman.getCartman().bate.x1 -= 20;
						Cartman.getCartman().bate.x2 -= 20;
					}
				}
			}

		}

		if (w.getPressedKeys().contains('d') || w.getPressedKeys().contains('D')) {// && !(w.getPressedKeys().contains('p'))) {
			if (pasarLvl2) {
				if (Cartman.getCartman().x1 < 1635) {
					Cartman.getCartman().moveDer();
					for (BolasDeNieve p : mapas.get(1).tiros) {
						if (p.name.equals("BalaCartman")) {
							p.x += 3;
						}
					}
				}
			} else if (pasarlvl3) {
				if (Cartman.getCartman().x2 < 1349) {
					Cartman.getCartman().moveDerPlus();
				}

			} else {
				if (Cartman.getCartman().x1 < 5320) {
					// boolean scroll = false;
					if (Cartman.getCartman().x1 < 4620) {
						if (Cartman.getCartman().x1 > maxScroll) {
							// scroll = true;
							// scroll positiu: A l'esquerra
							f.scroll(-3, 0);
							// has d'ajustar minscroll i maxscroll de la mateixa forma
							minScroll += 3;
							maxScroll += 3;
						}
					}

					Cartman.getCartman().moveDer();
					Cartman.getCartman().bate.moveDerB();
					if (bateReves) {
						Cartman.getCartman().currentImg = 0;
						bateReves = false;
						Cartman.getCartman().bate.x1 += 20;
						Cartman.getCartman().bate.x2 += 20;
					}

				}
			}
		}

		if (w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W')) {

			if (pasarLvl2) {
				if (Cartman.getCartman().y1 > 0) {
					Cartman.getCartman().moveArr();
					for (BolasDeNieve p : mapas.get(1).tiros) {
						if (p.name.equals("BalaCartman")) {
							p.y -= 3;
						}
					}
				}
			} else if (pasarlvl3) {

			} else {
				if (Cartman.getCartman().y1 > 733) {
					boolean flag = false;
					for (Enemigos e : mapas.get(0).enemigosList) {
						if (Cartman.getCartman().collidesWith(e)) {
							flag = true;
						}
					}
					Cartman.getCartman().moveArr();
					Cartman.getCartman().bate.moveArrB();
				}
			}
		}

		if (w.getPressedKeys().contains('s') || w.getPressedKeys().contains('S')) {
			if (pasarLvl2) {
				if (Cartman.getCartman().y2 < 958) {
					Cartman.getCartman().moveAba();
					for (BolasDeNieve p : mapas.get(1).tiros) {
						if (p.name.equals("BalaCartman")) {
							p.y += 3;
						}
					}
				}
			} else if (pasarlvl3) {

			} else {
				if (Cartman.getCartman().y2 < 960) {
					boolean flag = false;
					for (Enemigos e : mapas.get(0).enemigosList) {
						if (Cartman.getCartman().collidesWith(e)) {
							flag = true;
						}
					}
					Cartman.getCartman().moveAba();
					Cartman.getCartman().bate.moveAbaB();

				}
			}
		}
	}

}
