package SouthPark;

import java.util.Iterator;

/**
 * Esta clase esta creada basicamente para el movmimiento de los tiros
 * 
 * @author David Gil Vazquianez
 * @version 1.0
 *
 */
public class BolasDeNieve extends Sprite {
	// int cooldown=75;
	char direccion;
	int cooldown = 0;
	int x;
	int y;
	int id;

	public BolasDeNieve(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}

	public BolasDeNieve(String name, int x1, int y1, int x2, int y2, String path, char dir) {
		super(name, x1, y1, x2, y2, path);
		direccion = dir;

	}

	public BolasDeNieve(String name, int x1, int y1, int x2, int y2, int x, int y, int id, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
		this.id=id;
	}
	public BolasDeNieve(String name, int x1, int y1, int x2, int y2, int x, int y, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}

	/**
	 * Mover los tiros
	 */
	public void move() {
			x1 -= 10;
			x2 -= 10;
		
	}

	/**
	 * Borrar el tiro
	 */
	public void golpearBola() {
		delete();

	}

	/**
	 * Comprueba con que colisonan los tiros, si impacta un tiro con Cartman le baja
	 * la vida a parte de activar el movimiento del dano tanto de Cartman como del
	 * Bate
	 * 
	 * @param mapa
	 */
	public void colisionesTiros(Mapa mapa) {
		// TODO Auto-generated method stub
		// System.out.println("colisiones mapa");
		if (this.name.equals("BalaEnemigo")) {
			// System.out.println("Bala enemigos");
			if (!SouthPark.pasarLvl2) {
				if (this.collidesWith(Cartman.getCartman())) {
					Cartman.getCartman().hp--;
					System.out.println(Cartman.getCartman().hp);
					Cartman.getCartman().moveIzqDmg();
					Cartman.getCartman().bate.moveIzqDmgB();
					this.golpearBola();
				}
				if (this.collidesWith(Cartman.getCartman().bate)) {
					this.golpearBola();

				}
				for (Enemigos e : mapa.enemigosList) {
					if (this.collidesWith(e) && !e.name.equals("Garrison")) {
						this.golpearBola();
					}
				}
			} else if (SouthPark.pasarLvl2) {
				if (this.collidesWith(Cartman.getCartman())) {
					//Cartman.getCartman().hp--;
					System.out.println(Cartman.getCartman().hp);
					Cartman.getCartman().heridoBala(mapa);
					this.golpearBola();
				}
			}
		} else if (this.name.equals("BalaCartman")) {
			// System.out.println("bala cartman");
			for (Enemigos e : mapa.enemigosList) {
				if (this.collidesWith(e)) {
					// System.out.println("colisiono");
					e.heridoBalaEnemigo(mapa);
					this.golpearBola();
				}

			}
		}

	}

	public void moveBalasLvl2(int cx1, int cx2, int cy1, int cy2, int id) {

		// x=ratonX y=ratony cx1=X1personajec x2=X2personaje y1=Y1personaje
		// cy2=Y2personaje
		int cx = (cx1 + cx2) / 2; // cx=Centro del personaje en X
		int cy = (cy1 + cy2) / 2; // cy=Centro del personaje en Y
		int rx = x - cx; // rx=Resta des del raton al centro del personaje en X
		int ry = y - cy; // ry=Resta des del raton al centro del personaje en Y

		if (rx > 700 || rx < -700 || ry > 700 || ry < -700) {

			rx /= 3.5;
			ry /= 3.5;

		} else if (rx < 700 && rx >= 650 || rx <= -650 && rx > -700 || ry < 700 && ry >= 650
				|| ry <= -650 && ry > -700) {

			rx /= 3.25;
			ry /= 3.25;

		} else if (rx < 650 && rx >= 600 || rx <= -600 && rx > -650 || ry < 650 && ry >= 600
				|| ry <= -600 && ry > -650) {

			rx /= 3;
			ry /= 3;

		} else if (rx < 600 && rx >= 550 || rx <= -550 && rx > -600 || ry < 600 && ry >= 550
				|| ry <= -550 && ry > -600) {

			rx /= 2.75;
			ry /= 2.75;

		} else if (rx < 550 && rx >= 500 || rx <= -500 && rx > -550 || ry < 550 && ry >= 500
				|| ry <= -500 && ry > -550) {

			rx /= 2.5;
			ry /= 2.5;

		} else if (rx < 500 && rx >= 450 || rx <= -450 && rx > -500 || ry < 500 && ry >= 450
				|| ry <= -450 && ry > -500) {

			rx /= 2.25;
			ry /= 2.25;

		} else if (rx < 450 && rx >= 400 || rx <= -400 && rx > -450 || ry < 450 && ry >= 400
				|| ry <= -400 && ry > -450) {

			rx /= 2;
			ry /= 2;

		} else if (rx < 400 && rx >= 350 || rx <= -350 && rx > -400 || ry < 400 && ry >= 350
				|| ry <= -350 && ry > -400) {

			rx /= 1.75;
			ry /= 1.75;

		} else if (rx < 350 && rx >= 300 || rx <= -300 && rx > -350 || ry < 350 && ry >= 300
				|| ry <= -300 && ry > -350) {

			rx /= 1.5;
			ry /= 1.5;

		} else if (rx < 300 && rx >= 250 || rx <= -250 && rx > -300 || ry < 300 && ry >= 250
				|| ry <= -250 && ry > -300) {

			rx /= 1.25;
			ry /= 1.25;

		} else if (rx < 250 && rx >= 200 || rx <= -200 && rx > -250 || ry < 250 && ry >= 200
				|| ry <= -200 && ry > -250) {

		} else if (rx < 200 && rx >= 150 || rx <= -150 && rx > -200 || ry < 200 && ry >= 150
				|| ry <= -150 && ry > -200) {

			rx *= 1.25;
			ry *= 1.25;

		} else if (rx < 150 && rx >= 100 || rx <= -100 && rx > -150 || ry < 150 && ry >= 100
				|| ry <= -100 && ry > -150) {

			rx *= 1.5;
			ry *= 1.5;

		} else if (rx < 100 && rx >= 75 || rx <= -75 && rx > -100 || ry < 100 && ry >= 75 || ry <= -75 && ry > -100) {

			rx *= 2;
			ry *= 2;

		} else if (rx < 75 && rx >= 50 || rx <= -50 && rx > -75 || ry < 75 && ry >= 50 || ry <= -50 && ry > -75) {

			rx *= 2.5;
			ry *= 2.5;

		} else if (rx < 50 && rx >= 25 || rx <= -25 && rx > -50 || ry < 50 && ry >= 25 || ry <= -25 && ry > -50) {

			rx *= 5;
			ry *= 5;

		} else if (rx < 25 && rx > 0 || rx < 0 && rx > -25 || ry < 25 && ry > 0 || ry < 0 && ry > -25) {

			rx *= 10;
			ry *= 10;

		}
		if (rx > 0 && ry > 0) {
			// System.out.println("Abajo Derecha");
			if (this.name.equals("BalaCartman")) {
			//System.out.println("bala cartman");
			x1 += Math.abs(rx) * 0.03;
			x2 += Math.abs(rx) * 0.03;
			y1 += Math.abs(ry) * 0.03;
			y2 += Math.abs(ry) * 0.03;
			} else if (this.name.equals("BalaEnemigo")){
			//	System.out.println("bala enemigo");
				x1 += Math.abs(rx) * 0.017;
				x2 += Math.abs(rx) * 0.017;
				y1 += Math.abs(ry) * 0.017;
				y2 += Math.abs(ry) * 0.017;
				
			}
		} else if (rx < 0 && ry > 0) {
			// System.out.println("Abajo Izquierda");
			if (this.name.equals("BalaCartman")) {
				//System.out.println("bala cartman");
			x1 -= Math.abs(rx) * 0.03;
			x2 -= Math.abs(rx) * 0.03;
			y1 += Math.abs(ry) * 0.03;
			y2 += Math.abs(ry) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
				//System.out.println("bala enemigo");
				x1 -= Math.abs(rx) * 0.017;
				x2 -= Math.abs(rx) * 0.017;
				y1 += Math.abs(ry) * 0.017;
				y2 += Math.abs(ry) * 0.017;
			}

		} else if (rx > 0 && ry < 0) {
			// System.out.println("Arriba Derecha");
			if (this.name.equals("BalaCartman")) {
			//	System.out.println("bala cartman");
			x1 += Math.abs(rx) * 0.03;
			x2 += Math.abs(rx) * 0.03;
			y1 -= Math.abs(ry) * 0.03;
			y2 -= Math.abs(ry) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
				//System.out.println("bala enemigo");
				x1 += Math.abs(rx) * 0.017;
				x2 += Math.abs(rx) * 0.017;
				y1 -= Math.abs(ry) * 0.017;
				y2 -= Math.abs(ry) * 0.017;
			}

		} else if (rx < 0 && ry < 0) {
			// System.out.println("Arriba Izquierda");
			if (this.name.equals("BalaCartman")) {
			//	System.out.println("bala cartman");
			x1 -= Math.abs(rx) * 0.03;
			x2 -= Math.abs(rx) * 0.03;
			y1 -= Math.abs(ry) * 0.03;
			y2 -= Math.abs(ry) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
				//System.out.println("bala enemigo");
				x1 -= Math.abs(rx) * 0.017;
				x2 -= Math.abs(rx) * 0.017;
				y1 -= Math.abs(ry) * 0.017;
				y2 -= Math.abs(ry) * 0.017;
			}

		} else if (rx == 0 && ry > 0) {
			// System.out.println("Abajo");
			if (this.name.equals("BalaCartman")) {
			//	System.out.println("bala cartman");
			y1 += Math.abs(ry) * 0.03;
			y2 += Math.abs(ry) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
				//System.out.println("bala enemigo");
				y1 += Math.abs(ry) * 0.017;
				y2 += Math.abs(ry) * 0.017;
			}

		} else if (rx == 0 && ry < 0) {
			// System.out.println("Arriba");
			if (this.name.equals("BalaCartman")) {
				//System.out.println("bala cartman");
			y1 -= Math.abs(ry) * 0.03;
			y2 -= Math.abs(ry) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
			//	System.out.println("bala enemigo");
				y1 -= Math.abs(ry) * 0.017;
				y2 -= Math.abs(ry) * 0.017;
			}

		} else if (rx > 0 && ry == 0) {
			// System.out.println("Derecha");
			if (this.name.equals("BalaCartman")) {
				//System.out.println("bala cartman");
			x1 += Math.abs(rx) * 0.03;
			x2 += Math.abs(rx) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
			//	System.out.println("bala enemigo");
				x1 += Math.abs(rx) * 0.017;
				x2 += Math.abs(rx) * 0.017;
			}

		} else if (rx < 0 && ry == 0) {
			// System.out.println("Izquierda");
			if (this.name.equals("BalaCartman")) {
			//	System.out.println("bala cartman");
			x1 -= Math.abs(rx) * 0.03;
			x2 -= Math.abs(rx) * 0.03;
			} else if (this.name.equals("BalaEnemigo") && this.id ==id){
				//System.out.println("bala enemigo");
				x1 -= Math.abs(rx) * 0.017;
				x2 -= Math.abs(rx) * 0.017;

			}

		}
	}


	@Override
	public String toString() {
		return "BolasDeNieve [name= "+ name +" id= "+ id+" direccion=" + direccion + ", cooldown=" + cooldown + ", x=" + x + ", y=" + y +  "]";
	}

	public void update2() {
		if (cooldown < 250) {
			cooldown++;
		} else {
			if (!delete) {
				delete();
			}
		}

	}

}
