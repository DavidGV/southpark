package SouthPark;
/**
 * Esta clase la utilizo para poner el fondo y sirve
 * para crear terrenos.
 * 
 * @author David Gil Vazquianez
 * @version 1.0
 *
 */
public class Terreno extends Sprite{

	public Terreno(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		terrain = true;
	}
	public Terreno(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
		terrain = true;
	}
	
	

}
