package SouthPark;

import java.util.ArrayList;

public class Mapa {
	public Mapa(String name) {
	}
	/**
	 * Crear la lista de enemigos
	 */
	ArrayList<Enemigos> enemigosList = new ArrayList<>();
	/**
	 * Crear la lista de terrenos
	 */
	ArrayList<Terreno> tierras = new ArrayList<>();
	/**
	 * Crear la lista de tiros
	 */
	ArrayList<BolasDeNieve> tiros = new ArrayList<>();
	/**
	 * Crear la lista de sprites
	 */
	ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	public int nivel(int lvl) {
		if (!enemigosList.isEmpty()) {
			enemigosList.clear();
		} if (!tierras.isEmpty()) {
			tierras.clear();
		} if (!tiros.isEmpty()) {
			tiros.clear();
		} if (!enemigosList.isEmpty()) {
			sprites.clear();
		}
		return lvl;
	}
	public void vaciarListas() {
		enemigosList = new ArrayList<>();
		tierras = new ArrayList<>();
		tiros = new ArrayList<>();
		sprites = new ArrayList<Sprite>();	
	}
	
}
