package SouthPark;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Guardar extends SouthPark implements Serializable {
	ArrayList<Enemigos> enemigos;
	ArrayList<Terreno> terreno;
	ArrayList<BolasDeNieve> disparos;
	ArrayList<Sprite> sprites;
	int nivel;
	int gscore;
	int cx1;
	int cx2;
	int cy1;
	int cy2;
	int bx1;
	int bx2;
	int by1;
	int by2;
	int hp;
	int scrollx; 
	int scrolly;
	

	public Guardar(ArrayList<Enemigos> enemigos, ArrayList<Terreno> terreno, ArrayList<BolasDeNieve> disparos,
			ArrayList<Sprite> sprites, int nivel, int gscore, int cx1, int cx2, int cy1, int cy2, int hp) {
		this.enemigos = enemigos;
		this.terreno = terreno;
		this.disparos = disparos;
		this.sprites = sprites;
		this.nivel = nivel;
		this.gscore = gscore;
		this.cx1 = cx1;
		this.cx2 = cx2;
		this.cy1 = cy1;
		this.cy2 = cy2;
		
	}

	public Guardar(ArrayList<Enemigos> enemigos, ArrayList<Terreno> terreno, ArrayList<BolasDeNieve> disparos,
			ArrayList<Sprite> sprites, int nivel, int gscore, int cx1, int cx2, int cy1, int cy2, int bx1, int bx2,
			int by1, int by2, int hp, int scrollx, int scrolly) {
		this.enemigos = enemigos;
		this.terreno = terreno;
		this.disparos = disparos;
		this.sprites = sprites;
		this.nivel = nivel;
		this.gscore = gscore;
		this.cx1 = cx1;
		this.cx2 = cx2;
		this.cy1 = cy1;
		this.cy2 = cy2;
		this.bx1 = bx1;
		this.bx2 = bx2;
		this.by1 = by1;
		this.by2 = by2;
		this.hp = hp;
		this.scrollx = scrollx;
		this.scrolly = scrolly;
	}

	public Guardar(int nivel, int gscore) {
		this.nivel = nivel;
		this.gscore = gscore;
	}

	public static void guardar() throws IOException {
		// TODO Auto-generated method stub
		if (estoyLvl1) {
			ArrayList<ArrayList> lista = new ArrayList();
			lista.add(mapas.get(0).enemigosList);
			lista.add(mapas.get(0).tierras);
			lista.add(mapas.get(0).tiros);
			lista.add(mapas.get(0).sprites);
			write(lista, 1, SouthPark.score, Cartman.getCartman().x1, Cartman.getCartman().x2, Cartman.getCartman().y1,
					Cartman.getCartman().y2, Cartman.getCartman().bate.x1, Cartman.getCartman().bate.x2,
					Cartman.getCartman().bate.y1, Cartman.getCartman().bate.y2, Cartman.getCartman().hp, f.scrollx, f.scrolly);
			System.out.println("HP:"+Cartman.getCartman().hp);
		} else if (estoyLvl2) {
			ArrayList<ArrayList> lista = new ArrayList();
			lista.add(mapas.get(1).enemigosList);
			lista.add(mapas.get(1).tierras);
			lista.add(mapas.get(1).tiros);
			lista.add(mapas.get(1).sprites);
			write(lista, 2, SouthPark.score, Cartman.getCartman().x1, Cartman.getCartman().x2, Cartman.getCartman().y1,
					Cartman.getCartman().y2, 0, 0, 0, 0, Cartman.getCartman().hp, 0, 0);
		} else if (estoyLvl3) {
			ArrayList<ArrayList> lista = new ArrayList();
			lista.add(mapas.get(2).enemigosList);
			lista.add(mapas.get(2).tierras);
			lista.add(mapas.get(2).tiros);
			lista.add(mapas.get(2).sprites);
			write(lista, 3, SouthPark.score, Cartman.getCartman().x1, Cartman.getCartman().x2, Cartman.getCartman().y1,
					Cartman.getCartman().y2, 0, 0, 0, 0, Cartman.getCartman().hp, f.scrollx, f.scrolly);
		} else {
			ArrayList<ArrayList> lista = new ArrayList();
			lista.add(mapas.get(2).enemigosList);
			lista.add(mapas.get(2).tierras);
			lista.add(mapas.get(2).tiros);
			lista.add(mapas.get(2).sprites);
			write(lista, 0, SouthPark.score, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		}

	}

	public static void write(ArrayList<ArrayList> lista2, int i, int score, int x1, int x2, int y1, int y2, int bx1,
			int bx2, int by1, int by2, int hp, int scrollx, int scrolly) throws IOException {
		// TODO Auto-generated method stub
		File f = new File("resources/listaObjetos");
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		Guardar g = new Guardar(lista2.get(0), lista2.get(1), lista2.get(2), lista2.get(3), i, score, x1, x2, y1, y2,
				bx1, bx2, by1, by2, hp, scrollx, scrolly);
		System.out.println("gx1:" + g.cx1 + "gx2:" + g.cx2);
		oos.writeObject(g);
		oos.flush();
		// CERRAR ANTES DE VOLVER A ABRIR
		oos.close();
	}

	public static void cargar() throws IOException, ClassNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
		File f2 = new File("resources/listaObjetos");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Guardar g = (Guardar) ois.readObject();
		if (g.nivel == 1) {
			x = 1208;
			y = 307;
			Thread.sleep(150);
			SouthPark.score = g.gscore;
		} else if (g.nivel == 2) {
			pasarLvl2menu = true;
			x = 1208;
			y = 307;
			Thread.sleep(150);
			SouthPark.score = g.gscore;
		} else if (g.nivel == 3) {
			pasarLvl2menu = true;
			pasarlvl3menu = true;
			x = 1208;
			y = 307;
			Thread.sleep(150);
			SouthPark.score = g.gscore;
		} else {
			SouthPark.score = g.gscore;
			cargar = false;
		}
		ois.close();
	}

	public static void cargar2() throws IOException, ClassNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
		File f2 = new File("resources/listaObjetos");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Guardar g = (Guardar) ois.readObject();
		if (g.nivel == 1) {
			x = 1208;
			y = 307;
			Thread.sleep(150);
		} else if (g.nivel == 2) {
			x = 1258;
			y = 494;
			Thread.sleep(150);
		} else if (g.nivel == 3) {
			x = 1262;
			y = 691;
			Thread.sleep(150);
		}
		ois.close();

	}

	public static void cargar3() throws IOException, ClassNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
		File f2 = new File("resources/listaObjetos");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Guardar g = (Guardar) ois.readObject();
		if (g.nivel == 1) {
			mapas.get(0).enemigosList = g.enemigos;
			mapas.get(0).tierras = g.terreno;
			mapas.get(0).tiros = g.disparos;
			mapas.get(0).sprites = g.sprites;
			f.scroll(g.scrollx,g.scrolly);

		} else if (g.nivel == 2) {
			mapas.get(1).enemigosList = g.enemigos;
			mapas.get(1).tierras = g.terreno;
			mapas.get(1).tiros = g.disparos;
			mapas.get(1).sprites = g.sprites;

		} else if (g.nivel == 3) {
			mapas.get(2).enemigosList = g.enemigos;
			mapas.get(2).tierras = g.terreno;
			mapas.get(2).tiros = g.disparos;
			mapas.get(2).sprites = g.sprites;
			f.scroll(g.scrollx,g.scrolly);

		}
		cargar = false;
		ois.close();

	}

	public static void cargar4() throws IOException, ClassNotFoundException, InterruptedException {
		File f2 = new File("resources/listaObjetos");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Guardar g = (Guardar) ois.readObject();
		if (g.nivel == 1) {
			Cartman.getCartman().x1 = g.cx1;
			Cartman.getCartman().x2 = g.cx2;
			Cartman.getCartman().y1 = g.cy1;
			Cartman.getCartman().y2 = g.cy2;
			Cartman.getCartman().bate.x1 = g.bx1;
			Cartman.getCartman().bate.x2 = g.bx2;
			Cartman.getCartman().bate.y1 = g.by1;
			Cartman.getCartman().bate.y2 = g.by2;
			System.out.println("HPAntesdePoner:"+g.hp);
			Cartman.getCartman().hp = g.hp;
			System.out.println("HPDespues:"+Cartman.getCartman().hp);
		} else if (g.nivel == 2) {
			Cartman.getCartman().x1 = g.cx1;
			Cartman.getCartman().x2 = g.cx2;
			Cartman.getCartman().y1 = g.cy1;
			Cartman.getCartman().y2 = g.cy2;
			Cartman.getCartman().hp = g.hp;
		} else if (g.nivel == 3) {
			Cartman.getCartman().x1 = g.cx1;
			Cartman.getCartman().x2 = g.cx2;
			Cartman.getCartman().y1 = g.cy1;
			Cartman.getCartman().y2 = g.cy2;
			Cartman.getCartman().hp = g.hp;
		}
		ois.close();
	}
}
