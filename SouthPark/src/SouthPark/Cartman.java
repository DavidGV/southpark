package SouthPark;

import java.io.IOException;

/**
 * Esta clase esta creada basicamente para los movimientos de Cartman
 * 
 * @author David Gil Vazquianez
 * @version 1.2
 *
 */

public class Cartman extends Personajes {

	private static Cartman c = null;

	public static int nivel = 1;

	/**
	 * Sprites de Cartman
	 */
	static String[] spritesCartman3 = { "carmanDelReves.png" };
	static String[] spritesCartman = { "Cartman.gif", "cartmanReves.gif" };
	static String[] spritesAwesomo = { "awesomoArr.png", "awesomoAba.png", "awesomoDer.png", "awesomoIzq.png" };

	private Cartman() {
		super("Cartman", 50, 775, 125, 850, 0, spritesCartman);
		// TODO Auto-generated constructor stub
		direccion = 'd';
		cooldown = 0;
		cooldown2 = 0;
		hp = 3;
		bate = new Bate("Bate", 85, 760, 105, 835, 0, "bateMov.gif");

	}

	// metode get
	public static Cartman getCartman() {
		// si es null, es la primera vegada que es crida
		if (c == null) {
			c = new Cartman();
			return c;
			// si no es null, es que ja s'ha cridat abans
		} else {
			return c;
		}

	}

	int estat = 0; // 0 suelo, 1 subiendo 2 cayendo

	Bate bate;

	/**
	 * Mover Cartman a la Izquierda
	 */
	public void moveIzq() {
		x1 -= 3;
		x2 -= 3;
		direccion = 'a';
	}

	/**
	 * Mover Cartman a la Izquierda
	 */
	public void moveIzqPlus() {
		x1 -= 7;
		x2 -= 7;
		direccion = 'a';
	}

	/**
	 * Mover Cartman a la Derecha
	 */
	public void moveDer() {
		// TODO Auto-generated method stub
		x1 += 3;
		x2 += 3;
		direccion = 'd';
	}

	/**
	 * Mover Cartman a la Derecha
	 */
	public void moveDerPlus() {
		x1 += 7;
		x2 += 7;
		direccion = 'd';
	}

	/**
	 * Mover Cartman Arriba
	 */
	public void moveArr() {
		y1 -= 3;
		y2 -= 3;
		direccion = 'w';

	}

	/**
	 * Mover Cartman a Abajo
	 */
	public void moveAba() {
		y1 += 3;
		y2 += 3;
		direccion = 's';

	}

	/**
	 * Mover Cartman a la Izquierda cuando le golpean
	 */
	public void moveIzqDmg() {
		x1 -= 40;
		x2 -= 40;
		direccion = 'a';
	}

	/**
	 * Mover Cartman a la Derecha cuando le golpean
	 */
	public void moveDerDmg() {
		// TODO Auto-generated method stub
		x1 += 40;
		x2 += 40;
		direccion = 'd';
	}

	/**
	 * Mover Cartman Arriba cuando le golpean
	 */
	public void moveArrDmg() {
		y1 -= 40;
		y2 -= 40;
		direccion = 'w';

	}

	/**
	 * Mover Cartman Abajo cuando le golpean
	 */
	public void moveAbaDmg() {
		y1 += 40;
		y2 += 40;
		direccion = 's';

	}

	public void cambiarSpritesLvl2(int x, int y) {
		if (x < getCartman().x1) {
			getCartman().currentImg = 3;
		} else if (x > getCartman().x2) {
			getCartman().currentImg = 2;
		} else if (x > getCartman().x1 && x < getCartman().x2 && y < getCartman().y1) {
			getCartman().currentImg = 0;
		} else if (x > getCartman().x1 && x < getCartman().x2 && y > getCartman().y2) {
			getCartman().currentImg = 1;
		}
	}

	/**
	 * Actualiza el cooldown de los tiros (De momento no se utiliza)
	 */
	/*
	 * public void update() { if (cooldown < 25) cooldown++; }
	 */
	/**
	 * Cuando pulsas la tecla 'p' y cuando no estas en movimiento rotas el bate para
	 * atacar y cuando lo suelta el bate vuelve a su posicion original (pero cuando
	 * cartman esta mirando hacia la izquierda)
	 */
	public void atacarReves() {
		// TODO Auto-generated method stub
		if (SouthPark.w.getPressedKeys().contains('p')){
			if (!SouthPark.atkBate) {
				if (bate.cooldown == 25) {
					bate.cooldown = 0;
					bate.x1 -= 35;
					bate.x2 -= 35;
					bate.y1 += 20;
					bate.y2 += 20;
					bate.angle = 290;
					SouthPark.atkBate = true;
					SouthPark.bateReturns = true;
				}
			}
		} else if (SouthPark.bateReturns) {
			bate.x1 += 35;
			bate.x2 += 35;
			bate.y1 -= 20;
			bate.y2 -= 20;
			bate.angle = 0;
			SouthPark.atkBate = false;
			SouthPark.bateReturns = false;

		}
		bate.updateB();

	}

	public void atacar() {
		// TODO Auto-generated method stub
		if (SouthPark.w.getPressedKeys().contains('p')){
			if (!SouthPark.atkBate) {

				if (bate.cooldown == 25) {
					bate.cooldown = 0;
					bate.x1 += 35;
					bate.x2 += 35;
					bate.y1 += 20;
					bate.y2 += 20;
					bate.angle = 70;
					SouthPark.atkBate = true;
					SouthPark.bateReturns = true;
				}
			}
		} else if (SouthPark.bateReturns) {

			bate.x1 -= 35;
			bate.x2 -= 35;
			bate.y1 -= 20;
			bate.y2 -= 20;
			bate.angle = 0;
			SouthPark.atkBate = false;
			SouthPark.bateReturns = false;
		}
		bate.updateB();

	}

	/**
	 * Comprueba que los enemigos tocan a Cartman y si lo tocan le baja la vida a
	 * parte de activar el movimiento del dano tanto de Cartman como del Bate
	 * 
	 * @param mapa
	 */
	public void herido(Mapa mapa) {
		// TODO Auto-generated method stub
		for (Enemigos e : mapa.enemigosList) {
			if (collidesWith(e)) {
				if (e.currentImg == 0) {
					System.out.println("da�o arriba ENE2:" + hp);
					hp--;
					System.out.println(hp);
					moveIzqDmg();
					bate.moveIzqDmgB();
				} else {
					System.out.println("da�o arriba ENE2:" + hp);
					hp--;
					System.out.println(hp);
					moveDerDmg();
					bate.moveDerDmgB();
				}
			}

		}

	}

	public void chocar(Mapa mapa) throws InterruptedException {
		for (Enemigos e : mapa.enemigosList) {
			if (collidesWith(e) && e.name.equals("win")) {
				SouthPark.ganar3 = true;
			} else if (collidesWith(e)) {
				x1 = 825;
				x2 = 950;
				y1 = 75;
				y2 = 175;
				hp--;
				SouthPark.f.resetScroll();
				Thread.sleep(200);
			}
		}
	}

	/**
	 * Comprueba que los enemigos tocan a Cartman y si lo tocan le baja la vida a
	 * parte de activar el movimiento del dano tanto de Cartman como del Bate
	 */
	public void herido2(Mapa mapa) {
		// TODO Auto-generated method stub
		for (Enemigos e : mapa.enemigosList) {
			if (collidesWith(e)) {
				if (this.getCollisionType(e).contains("u")) {
					// System.out.println("golpea por arriba --> baja");
					hp--;
					// System.out.println("da�o arriba ENE:"+hp);
					moveAbaDmg();
				} else if (this.getCollisionType(e).contains("d")) {
					// System.out.println(" por abajo --> sube");
					hp--;
					System.out.println("da�o abajo ENE:" + hp);
					moveArrDmg();
				} else if (this.getCollisionType(e).contains("l")) {// arriba
					// System.out.println("golpea por la izquierda --> va para la derecha");
					hp--;
					// System.out.println("da�o izquierda ENE:"+hp);
					moveDerDmg();
				} else if (this.getCollisionType(e).contains("r")) {// abajo
					// System.out.println("golpea por la derecha --> va para la izquierda");
					hp--;
					// System.out.println("da�o derecha ENE:"+hp);
					moveIzqDmg();
				}
			}
		}
	}

	public void heridoBala(Mapa mapa) {
		// TODO Auto-generated method stub
		for (BolasDeNieve t : mapa.tiros) {
			if (collidesWith(t)) {
				if (this.getCollisionType(t).contains("u")) {
					// System.out.println("golpea por arriba --> baja");
					hp--;
					// System.out.println("da�o arriba:"+hp);
					moveAbaDmg();
				} else if (this.getCollisionType(t).contains("d")) {
					// System.out.println("golpea por abajo --> sube");
					hp--;
					// System.out.println("da�o abajo:"+hp);
					moveArrDmg();
				} else if (this.getCollisionType(t).contains("l")) {// arriba
					// System.out.println("golpea por la izquierda --> va para la derecha");
					hp--;
					// System.out.println("da�o izquierda:"+hp);
					moveDerDmg();
				} else if (this.getCollisionType(t).contains("r")) {// abajo
					// System.out.println("golpea por la derecha --> va para la izquierda");
					hp--;
					// System.out.println("da�o derecha:"+hp);
					moveIzqDmg();
				}
			}
		}
	}

	/**
	 * Cambia el valor de las vidas
	 */

	public void marcadorVidas() {
		// TODO Auto-generated method stub
		if (hp == 2) {
			SouthPark.corazones.currentImg = 1;
		}
		if (hp == 1) {
			SouthPark.corazones.currentImg = 2;
		}
	}

	public void nivel(int i) throws ClassNotFoundException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		this.nivel = i;
		switch (i) {
		case 1:
			// TODO Auto-generated constructor stub
			this.imgArray = spritesCartman;
			bate = new Bate("Bate", 85, 760, 105, 835, 0, "bateMov.gif");
			if (!SouthPark.cargar) {
				x1 = 50;
				y1 = 775;
				x2 = 125;
				y2 = 850;
				hp = 3;
			} else {
				Guardar.cargar4();
			}
			direccion = 'd';
			cooldown = 25;
			break;
		case 2:
			// TODO MAS
			this.imgArray = spritesAwesomo;
			if (!SouthPark.cargar) {
				x1 = 825;
				x2 = 950;
				y1 = 475;
				y2 = 575;
				hp = 3;
			} else {
				Guardar.cargar4();
			}
			this.bate = null;
			break;
		case 3:
			// TODO MAS
			this.imgArray = spritesCartman3;
			if (!SouthPark.cargar) {
				x1 = 825;
				x2 = 950;
				y1 = 175;
				y2 = 275;
				hp = 3;
			}else {
				Guardar.cargar4();
			}
			this.bate = null;

		}

	}

	public BolasDeNieve shoot(Mapa mapa, int x, int y) {
		// System.out.println("x:"+x+" y:"+y);
		// TODO Auto-generated method stub
		if (cooldown == 50) {
			cooldown = 0;
			BolasDeNieve bolaNieve = new BolasDeNieve("BalaCartman", ((x1 + x2) / 2) - 10, ((y1 + y2) / 2) - 10,
					((x1 + x2) / 2) + 10, ((y1 + y2) / 2) + 10, x, y, "bola2.png");
			mapa.tiros.add(bolaNieve);
			return bolaNieve;
		} else {
			return null;
		}

	}

	public void update() {
		if (cooldown < 50)
			cooldown++;
	}

}
